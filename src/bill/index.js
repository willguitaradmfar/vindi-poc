
const Vindi = require('vindi-node-sdk')

module.exports = {

    async create(data){
        return await Vindi.bill.create(data)
    }

}