const {
  create: createCustomer,
  findByCode: findByCodeCustomer
} = require("./customer");

const { create: createPaymentProfile } = require("./payment-profile");

const { create: createBill } = require("./bill");

const { findAll: findAllPaymentMethod } = require("./payment_methods");

const {
  findAll: findAllProduct,
  findByCode: findByCodeProduct,
  create: createProduct
} = require("./product");

module.exports = async () => {
  // await findAllPaymentMethod()

  const productDraft = {
    code: "0010.1234.4321",
    name: "Kit Bemacash 0010.1234.4321",
    price: 1234.56
  };

  let product = await findByCodeProduct(productDraft.code);

  if (!product) {
    await createProduct({
      name: productDraft.name,
      code: productDraft.code,
      //unit: "",
      status: "active", // ['active' ou 'inactive' ou 'deleted']:
      description: productDraft.name,
      // invoice: "",
      pricing_schema: {
        price: productDraft.price,
        //minimum_price: productDraft.price,
        schema_type: "per_unit"
      },
      metadata: "metadata"
    });

    product = await findByCodeProduct(productDraft.code);
  }

  const registry_code = "19143584730";

  let customer = await findByCodeCustomer("T9998")

  if (!customer) {
    await createCustomer({
      name: "Maria das Dores (Totvs-store)",
      email: "maria@store.totvs.com",
      registry_code,
      code: "T9998", // unico CodigoT se for cliente novo ?
      notes: "Observação teste",
      metadata: "Store",
      address: {
        street: "Praça da Sé",
        number: "001",
        additional_details: "lado ímpar",
        zipcode: "01001000",
        neighborhood: "Sé",
        city: "São Paulo",
        state: "SP",
        country: "BR"
      },
      phones: [
        {
          phone_type: "mobile", //['mobile' ou 'landline']
          number: "55119999-9999", // E.164
          extension: "" // ramal
        }
      ]
    })

    customer = await findByCodeCustomer("T9998")
  }

  const paymentProfile = await createPaymentProfile({
    holder_name: "TITULAR CARTAO 2",
    registry_code: registry_code,
    //bank_branch: "0001",
    //bank_account: "06410122",
    card_expiration: "01/23",
    card_number: "5555 5555 5555 5557",
    card_cvv: "123",
    payment_method_code: "credit_card",
    //payment_company_code: "",
    //gateway_token: "",
    customer_id: customer.id
  });

  return await createBill({
    customer_id: customer.id,
    code: "19.1C15.1236",
    installments: 1,
    payment_method_code: "credit_card",
    //billing_at: "",
    //due_at: "",
    bill_items: [
      {
        product_id: product.id,
        //product_code: "",
        amount: productDraft.price,
        description: "Bemacash gestão",
        //quantity: 1
        // pricing_schema: {
        //   price: 51.67,
        //   minimum_price: 0,
        //   schema_type: "per_unit"
        //   // pricing_ranges: [
        //   //   {
        //   //     start_quantity: 0,
        //   //     end_quantity: 0,
        //   //     price: 0,
        //   //     overage_price: 0
        //   //   }
        //   // ]
        // }
      }
    ],
    metadata: "metadata"
    // payment_profile: {
    //   id: 0,
    //   holder_name: "",
    //   registry_code: "",
    //   bank_branch: "",
    //   bank_account: "",
    //   card_expiration: "",
    //   card_number: "",
    //   card_cvv: "",
    //   payment_method_code: "",
    //   payment_company_code: "",
    //   gateway_token: ""
    // },
    // payment_condition: {
    //   penalty_fee_value: 0,
    //   penalty_fee_type: "",
    //   daily_fee_value: 0,
    //   daily_fee_type: "",
    //   after_due_days: 0,
    //   payment_condition_discounts: [
    //    {
    //      value: 0,
    //       value_type: "",
    //       days_before_due: 0
    //     }
    //   ]
    // }
  });
};
