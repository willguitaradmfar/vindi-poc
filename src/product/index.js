
const Vindi = require('vindi-node-sdk')

module.exports = {

    async findAll(data){
        return await Vindi.product.all(data)
    },

    async findByCode(code){
        const all =  await Vindi.product.all({
            query: `code=${code}`
        })

        return all.items.length ? all.items[0] : undefined
    },

    async create(data){
        return await Vindi.product.create(data)
    }

}