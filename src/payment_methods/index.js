
const Vindi = require('vindi-node-sdk')

module.exports = {

    async findAll(data){
        return await Vindi.paymentMethod.all(data)
    }

}