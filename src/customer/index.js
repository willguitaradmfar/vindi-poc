
const Vindi = require('vindi-node-sdk')

module.exports = {

    async create(data){
        return await Vindi.customer.create(data)
    },

    async findByCode(code){
        const all =  await Vindi.customer.all({
            query: `code=${code}`
        })

        return all.items.length ? all.items[0] : undefined
    }

}